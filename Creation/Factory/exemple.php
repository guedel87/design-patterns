<?php
abstract class BaseClass 
{
  public abstract function doing();
}

class ClasseA extends BaseClass 
{
  public function doing() {
    echo "this is A", PHP_EOL;
  }
}

class ClasseB extends BaseClass 
{
  public function doing() {
    echo "this is B", PHP_EOL;
  }
}

class UnknownCodeException extends \Exception 
{

}

class Factory 
{
  public function createClass(string $code) : BaseClass 
  {
    if ($code == "A") {
      return new ClasseA();
    } elseif ($code == "B") {
      return new ClasseB();
    }
    throw new UnknowCodeException();
  }
}

/* Sample Use */
$f = new Factory();
$o1 = $f->createClass("A");
$o2 = $f->createClass("B");

$o1->doing();
$o2->doing();
