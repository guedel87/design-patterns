<?php

/*
 * Fabrique abstraite
 */
abstract class GUIFactory {
    public static function getFactory() {
        $sys = readFromConfigFile("OS_TYPE");
        if ($sys == 0) {
            return(new WinFactory());
        } else {
            return(new OSXFactory());
        }
    }
    public abstract function createButton();
}

class WinFactory extends GUIFactory {
    public function createButton() {
        return(new WinButton());
    }
}

class OSXFactory extends GUIFactory {
    public function createButton() {
        return(new OSXButton());
    }
}

abstract class Button {
    private $_caption;
    public abstract function render();

    public function getCaption(){
        return $this->_caption;
    }
    public function setCaption($caption){
        $this->_caption = $caption;
    }
}

class WinButton extends Button {
    public function render() {
        return "Je suis un WinButton: ".$this->getCaption();
    }
}

class OSXButton extends Button {
    public function render() {
        return "Je suis un OSXButton: ".$this->getCaption();
    }
}

$aFactory = GUIFactory::getFactory();
$aButton = $aFactory->createButton();
$aButton->setCaption("Démarrage");
echo $aButton->render();

// affiche :
//   Je suis un WinButton: Démarrage
// ou :
//   Je suis un OSXButton: Démarrage