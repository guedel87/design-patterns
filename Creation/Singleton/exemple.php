<?php

    class Singleton {
        private function __construct() {
        }

        private function __clone() {}

        public static function getInstance() {
            static $instance = null;

            if ($instance===null) {
                $instance = new self();
            }
            return $instance;
        }


        /**
         * Exemple de méthode métier
         */
        public function action() {}
    }

    // Exemple d'appel
    Singleton::getInstance()->action();
