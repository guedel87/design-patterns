<?php

    /**
     * Description of Prototype
     *
     * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>
     */
    abstract class Prototype
    {
        /**
         * @return Prototype
         */
        public abstract function clone_();
    }

    class PrototypeA extends Prototype
    {
        /**
         * @return Prototype
         */
        public function clone_() { }
    }

    class PrototypeB extends Prototype
    {
        /**
         * @return Prototype
         */
        public function clone_() { }
    }
