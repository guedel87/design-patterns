<?php
/*
 * Source: https://www.tutorialspoint.com/design_pattern/mediator_pattern.htm
 * 
 */


/**
 * Classe médiateur
 */
class ChatRoom {
  public static function showMessage(User $user, string $message) 
  {
    echo date(DateTime::ATOM),  " [", $user->getName(), "] ", $message, PHP_EOL;
  }
}

class User 
{
  private string $name;


  public function __construct($name)
  {
    $this->name = $name;
  }
  
  public function getName()
  {
    return $this->name;
  }
  
  public function sendMessage($message)
  {
    ChatRoom::showMessage($this, $message);
  }
}


/* sample use */
$u1 = new User("Roger");
$u2 = new User("Fanny");

$u1->sendMessage("Salut Fanny");
$u2->sendMessage("bonjour Roger");
