# Description

Le visiteur est très utile pour parcourir un arbre. Le mode de descente le plus
utilisé est "en profondeur d'abord".

On peut par exemple parcourir un document XML, un fichier YAML.

# Inconvénients

Bien que assez facile à mettre en oeuvre, il s'agit d'une opération récursive qui peut
exploser la mémoire si le volume de données est important.