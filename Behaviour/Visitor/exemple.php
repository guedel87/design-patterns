<?php

abstract class Element {
   protected $name;

   public function __construct(string $name)
   {
     $this->name = $name;
   }
   
   public function getName()
   {
     return $this->name;
   }
   
   abstract public function accept(ElementVisitor $visitor);
}

class Terminal extends Element
{
  private $value;
  
  public function __construct(string $name, $value)
  {
    parent::__construct($name);
    $this->value = $value;
  }
   
  public function accept(\ElementVisitor $visitor)
  {
    $visitor->visitTerminal($this);
  }
  
  public function getValue()
  {
    return $this->value;
  }

}

class NonTerminal extends Element {
  private $elements;
  
  public function __construct(string $name, Element ...$elements)
  {
    parent::__construct($name);
    $this->elements = $elements;
  }

  public function accept(\ElementVisitor $visitor)
  {
    $visitor->visitNonTerminal($this);
  }
  
  public function getChilds()
  {
    return $this->elements;
  }

}

class ElementVisitor
{
  private $indent = 0;
  public function visitTerminal(Terminal $t)
  {
    echo str_repeat(" ", $this->indent);
    echo $t->getName(), " = ", $t->getValue(), PHP_EOL;
  }
  
  public function visitNonTerminal(NonTerminal $n)
  {
    $space = str_repeat(" ", $this->indent);
    echo $space;
    echo $n->getName(), "= {", PHP_EOL;
    $this->indent++;
    foreach($n->getChilds() as $child) {
      $child->accept($this);
    }
    $this->indent--;
    echo $space;
    echo "}", PHP_EOL;
  }
}

/*
 * Sample use
 */
$g = new NonTerminal("l1", 
    new Terminal("t1", 1), 
    new NonTerminal("l2", new Terminal("t2", 2)),
    new Terminal("t3", 3)
);

$v = new ElementVisitor();
$g->accept($v);
