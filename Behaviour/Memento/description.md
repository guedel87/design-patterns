# Principe

Le mémento sert à stocker l'état d'un objet afin de le restaurer ultérieurment.

Un exemple couramment rencontré est le fameux "Undo" des éditeurs.

Le gardien conserve une liste de mémento. Il permet de restaurer un objet à un état précédement sauvegardé dans le gardien.


