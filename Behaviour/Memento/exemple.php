<?php

class Originator
{
  private string $state;
  
  public function setState($state)
  {
    echo __CLASS__ , ": état affecté à $state", PHP_EOL;
    $this->state = $state;
  }
  
  public function saveToMemento()
  {
    echo __CLASS__, ": sauvegardé dans le mémento.", PHP_EOL;
    return new Memento($this->state);
  }
  
  public function restoreFromMemento(Memento $m)
  {
    $this->state = $m->getSavedState();
    echo __CLASS__ , ": état apres restauration $this->state", PHP_EOL;
  }
}

class Memento
{
  private string $state;
  public function __construct($state)
  {
    $this->state = $state;
  }
  public function getSavedState(): string
  {
    return $this->state;
  }
}

class CareTaker
{
  private $savedStates = [];
  public function addMemento(Memento $m)
  {
    $this->savedStates[] = $m;
  }
  public function getMemento(int $index) 
  {
      return $this->savedStates[$index];
  }
}

/* sample use */
$caretaker = new CareTaker();
$originator = new Originator();
$originator->setState("state1");
$originator->setState("state2");
$caretaker->addMemento($originator->saveToMemento());
$originator->setState("state3");
$caretaker->addMemento($originator->saveToMemento());
$originator->setState("state4");
$originator->restoreFromMemento( $caretaker->getMemento(1));
