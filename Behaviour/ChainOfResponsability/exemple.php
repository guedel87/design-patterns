<?php
/**
 * Source: https://fr.wikibooks.org/wiki/Patrons_de_conception/Cha%C3%AEne_de_responsabilit%C3%A9
 */
abstract class Logger
{
  const ERR=0;
  const NOTICE=1;
  const DEBUG=2;
  
  protected int $level;
  
  protected ?Logger $next;
  
  protected function __construct(int $level)
  {
    $this->level = $level;
    $this->next = null;
  }
  
  public function setNext(Logger $l): Logger
  {
    $this->next = $l;
    return $l;
  }
  
  public function message(string $msg, int $priority)
  {
    if ($priority <= $this->level) {
      $this->writeMessage($msg);
    }
    if ($this->next !== null) {
       $this->next->message($msg, $priority);
    }
  }
  
  abstract protected function writeMessage(string $msg): void;
}

class StdOutLogger extends Logger
{
  public function __construct(int $level)
  {
    parent::__construct($level);
  }

  public function writeMessage(string $msg): void
  {
    echo "writing to stdout: ", $msg, PHP_EOL;
  }
}

class EmailLogger extends Logger {
  public function __construct(int $level)
  {
    parent::__construct($level);
  }
  
  public function writeMessage(string $msg): void
  {
    echo "Sending via email: ", $msg, PHP_EOL;
  }
}

class FileLogger extends Logger {
   private $filename;
  public function __construct(int $level, string $filename)
  {
    parent::__construct($level);
    $this->filename = $filename;
  }
  
  public function writeMessage(string $msg): void
  {
    echo "Writing to file ", $this->filename, " : ", $msg, PHP_EOL;
  }
}

/*
 * Sample use
 */
$l = new StdOutLogger(Logger::DEBUG);
$l
    ->setNext(new EmailLogger(Logger::NOTICE))
    ->setNext(new FileLogger(Logger::ERR, "sample.log"));
    ;

 $l->message("Entering function y", Logger::DEBUG);
 $l->message("Step 1 completed", Logger::NOTICE);
 $l->message("An error has occured", Logger::ERR);
 