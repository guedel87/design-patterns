# Description

L'idée est de crée une classe distincte par état d'un objet. Ainsi l'object connait parfaitement son contexte et
peut agir en conséquence. Cela évite une multitude de "switch" pour tester l'état d'un object pour définir son comportement.

# Exemple
L'exemple utilisé prend différents état d'un fauteuil. S'il est occupé alors on peut le chauffer.
