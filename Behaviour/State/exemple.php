<?php

abstract class Place
{
  abstract public function faire();
}

class PlaceOccupee extends Place
{
  
  public function faire()
  {
    echo "on chauffe", PHP_EOL;
  }

}

class PlaceVide extends Place
{
  
  public function faire()
  {
    echo "je suis pliée", PHP_EOL;
  }

}

$p1 = new PlaceOccupee();
$p2 = new PlaceVide();

$p1->faire();
$p2->faire();
