# Description

Il s'agit ici de définir un fonctionnement élémentaire dans le patron de méthode. Elle contient au moins une méthode virtuelle qui sera développée dans les classe filles et qui modifiera le comportement.

# Exemple: Jeu de société

Par exemple, les jeux de société ont le développement d'une partie commune. On commence par définir le nombre de joueur. Ensuite on met en place le plateau. On définit le mode de déplacement des pions. On détermine la victoire d'un joueur.

Si on joue au petits cheveaux ou au Monopoly la trame générale de déroulement du jeu est similaire.
