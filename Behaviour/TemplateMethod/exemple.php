<?php

abstract class Horloge
{
  protected int $valeur;


  public function __construct(int $valeurInitiale)
  {
    $this->valeur = $valeurInitiale;
  }
  
  public function incremente()
  {
    $this->valeur ++;
  }
  
  public abstract function affiche();
}

class HorlogeNumerique extends Horloge
{
  
  public function affiche()
  {
    echo $this->valeur, PHP_EOL;
  }

}

class HorlogeAnalogique extends Horloge
{
  
  public function affiche()
  {
    echo '(', $this->valeur * 2, ')', PHP_EOL;
  }
}

/* sample use */
$h1 = new HorlogeNumerique(1);
$h2 = new HorlogeAnalogique(1);

$h1->incremente();
$h2->incremente();

$h1->affiche();
$h2->affiche();
