<?php
interface Strategy
{
  public function execute();
}

class Algo1 implements Strategy
{
  
  public function execute()
  {
    echo "première stratégie", PHP_EOL;
  }

}

class Algo2 implements Strategy
{
  
  public function execute()
  {
    echo "deuxième stratégie", PHP_EOL;
  }

}

class Algo3 implements Strategy
{
  
  public function execute()
  {
    echo "troisième stratégie", PHP_EOL;
  }

}

class Player
{
  private Strategy $strategy;
  
  public function __construct(Strategy $strategy)
  {
    $this->setStrategy($strategy);
  }
  
  public function setStrategy(Strategy $strategy)
  {
    $this->strategy = $strategy;
  }
  
  public function execute()
  {
    $this->strategy->execute();
  }
}

/* sample use */
$s1 = new Algo1();
$s2 = new Algo2();
$s3 = new Algo3();

$p1 = new Player($s1);
$p1->execute();

$p1->setStrategy($s2);
$p1->execute();

$p1->setStrategy($s3);
$p1->execute();
