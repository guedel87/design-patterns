<?php
/**
 * source: https://fr.wikibooks.org/wiki/Patrons_de_conception/Interpr%C3%A9teur
 */


interface Expression {
  public function interpret(SplStack $stack) : void;
}

class TerminalExpression_Number implements Expression
{
  private int $number;
  
  public function __construct(int $number)
  {
    $this->number = $number;
  }


  public function interpret(SplStack $stack): void
  {
    $stack->push($this->number);
  }

}

class TerminalExpression_Plus implements Expression
{
  public function interpret(SplStack $stack): void
  {
    $stack->push($stack->pop() + $stack->pop());
  }
  
}

class TerminalExpression_Minus implements Expression
{
  public function interpret(SplStack $stack): void
  {
    $n1 = $stack->pop();
    $n2 = $stack->pop();
    $stack->push($n2 - $n1);
  }
}

class Parser
{
  private $parseTree = [];
  
  public function __construct(String $s)
  {
    foreach (explode(" ", $s) as $token) {
      if ($token === "+") {
        $this->parseTree[] = new TerminalExpression_Plus();
      } elseif ($token === "-") {
        $this->parseTree[] = new TerminalExpression_Minus();
      } else {
        $this->parseTree[] = new TerminalExpression_Number((int) $token);
     }
    }
  }
  
  public function evaluate(): int
  {
    $context = new SplStack();
    foreach ($this->parseTree as $e) {
      $e->interpret($context);
    }
    return $context->pop();
  }
}

/* Sample use */
$expression = "42 4 2 - +";
$parser = new Parser($expression);
echo "'$expression' equals ", $parser->evaluate();
