<?php

class Interrupteur
{

  private Command $flipUpCommand;
  private Command $flipDownCommand;

  public function __construct(Command $flipUpCommand, Command $flipDownCommand)
  {
    $this->flipUpCommand = $flipUpCommand;
    $this->flipDownCommand = $flipDownCommand;
  }

  public function leve()
  {
    $this->flipUpCommand->execute();
  }

  public function abaisse()
  {
    $this->flipDownCommand->execute();
  }

}

/**
 * Récepteur
 */
class Lumiere
{
  public function allume() 
  {
    echo "La lumière est allumée", PHP_EOL;
  }
  
  public function eteint()
  {
    echo "La lumière est éteinte", PHP_EOL;
  }
}

/**
 * Commande
 */
interface Command
{

  public function execute(): void;
}

/**
 * Commande concrète pour allumer la lumière
 */
class TurnOnCommand implements Command
{
  private Lumiere $light;
  
  public function __construct(Lumiere $light)
  {
    $this->light = $light;
  }

  public function execute(): void
  {
    $this->light->allume();
  }

}
/**
 * Commande concrète pour éteindre la lumière
 */
class TurnOffCommand implements Command
{
  private Lumiere $light;
  
  public function __construct(Lumiere $light)
  {
    $this->light = $light;
  }

  public function execute(): void
  {
    $this->light->eteint();
  }

}

/* sample use */
$lamp = new Lumiere();
$allume = new TurnOnCommand($lamp);
$eteint = new TurnOffCommand($lamp);

$s = new Interrupteur($allume, $eteint);

$s->leve();
$s->abaisse();
