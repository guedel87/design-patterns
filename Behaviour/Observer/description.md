# Description

L'idée est de pouvoir envoyer une notification à des abonnés.

Ce peut être par exemple des services qui sont en attente de notifications de changemment d'état.

L'avantage de passer par une souscription est qu'on sait à qui envoyer la notification. Les abonnées fournissent une méthode pour la notification.
