<?php

interface Souscripteur {
  public function notifier($notification);
}

class Publicateur {
  /**
   *
   * @var ArrayObject
   */
  private $souscripteurs;
  
  public function __construct()
  {
    $this->souscripteurs = new ArrayObject();
  }
  
  public function inscription(Souscripteur $s)
  {
    $this->souscripteurs->append($s);
  }
  
  public function resiliation(Souscripteur $s)
  {
    foreach ($this->souscripteurs as $idx => $sous) {
      if ($s = $sous) {
        unset($this->souscripteurs[$idx]);
      }
    }
  }
  
  public function notifierSouscripteurs($notification = null)
  {
    foreach($this->souscripteurs as $s) {
      $s->notifier($notification);
    }
  }
}

class Souscripteur1 implements Souscripteur
{
  
  public function notifier($notification)
  {
    echo __CLASS__, " reçu notification $notification", PHP_EOL;
  }

}

class Souscripteur2 implements Souscripteur
{
  
  public function notifier($notification)
  {
    echo __CLASS__, " reçu notification $notification", PHP_EOL;
  }
}

/* sample use */
$pub = new Publicateur();
$s1 = new Souscripteur1();
$s2 = new Souscripteur2();
$pub->inscription($s1);
$pub->inscription($s2);
$pub->notifierSouscripteurs("UP");
$pub->resiliation($s2);
$pub->notifierSouscripteurs("DOWN");

