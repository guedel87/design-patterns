<?php

interface IAdapter
{
    public function query();
}

class Adapter1 implements IAdapter
{
    public function query()
    {
        echo "adapter1";
    }
}

class Adapter2 implements IAdapter
{
    public function query()
    {
        echo "adapter2";
    }
}


class Adapted
{
    /**
     *
     * @var IAdapter
     */
    private $adapter;

    public function __construct()
    {
        $this->adapter = new Adapter1();
    }

    public function specificQuery()
    {
        $this->adapter->query();
    }
}