<?php

class Component
{
    // Attributs
    private $basePath;
    private $name;
    private $parent;

    public function __construct($name, CDirectory $parent = null)
    {
        // Debug : echo "constructor Component";
        $this->name = $name;
        $this->parent = $parent;
        if($this->parent != null)
        {
            $this->parent->addChild($this);
            $this->basePath = $this->parent->getPath();
        }
        else
        {
            $this->basePath = '';
        }
    }

    // Getters
    public function getBasePath() { return $this->basePath; }
    public function getName() { return $this->name; }
    public function getParent() { return $this->parent; }

    // Setters
    public function setBasePath($basePath) { $this->basePath = $basePath; }
    public function setName($name) { $this->name = $name; }
    public function setParent(CDirectory $parent) { $this->parent = $parent; }

    // Méthode
    public function getPath() { return $this->getBasePath().'/'.$this->getName(); }
}

class CFile extends Component
{
    // Attributs
    private $type;

    public function __construct($name, $type, CDirectory $parent = null)
    {
        // Debug : echo "constructor CFile";
        $this->type = $type;

        // Retrieve constructor of Component
        parent::__construct($name, $parent);
    }

    // Getters
    public function getType() { return $this->type; }

    // Setters
    public function setType($type) { $this->type = $type; }

    // Méthodes de la classe Component
    public function getName() { return parent::getName().'.'.$this->getType(); }
    public function getPath() { return parent::getPath().'.'.$this->getType(); }
}

class CDirectory extends Component
{

    // Attributs
    private $childs;

    public function __construct($name, CDirectory $parent = null)
    {
        // Debug : echo "constructor CDirectory";
        $this->childs = array();

        // Retrieve constructor of Component
        parent::__construct($name, $parent);
    }

    // Getters
    public function getChilds() { return $this->childs; }

    // Setters
    public function setChilds($childs) { $this->childs = $childs; }

    // Méthodes
    public function addChild(Component $child)
    {
        $child->setParent($this);
        $this->childs[] = $child;
    }

    public function showChildsTree($level = 0)
    {
        echo "<br/>".str_repeat('&nbsp;', $level).$this->getName();
        foreach($this->getChilds() as $child)
        {
            if($child instanceof self)
            {
                $child->showChildsTree($level+1);
            }
            else
            {
                echo "<br/>".str_repeat('&nbsp;', $level+1).$child->getName();
            }
        }
    }
}

// Exemple d'utilisation
$root = new CDirectory('root');
$dir1 = new CDirectory('dir1', $root);
$dir2 = new CDirectory('dir2', $root);
$dir3 = new CDirectory('dir3', $root);
$dir4 = new CDirectory('dir4', $dir2);
$file1 = new CFile('file1','txt', $dir1);
$file2 = new CFile('doc', 'pdf', $dir4);

$root->showChildsTree();