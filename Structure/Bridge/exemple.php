<?php
    /*
     * Exemple de structure en pont
     */

    /**
     * Implementation
     */
    interface DrawingAPI
    {
        /**
         *
         * @param double $x
         * @param double $y
         * @param double $radius
         */
        public function drawCircle($x, $y, $radius);
    }

    /**
     * Implémentation 1
     */
    class DrawingAPI1 implements DrawingAPI
    {
        public function drawCircle($x, $y, $radius)
        {
            echo "API1.cercle position $x:$y rayon $radius";
        }
    }

    /**
     * Implémentation 2
     */
    class DrawingAPI2 implements DrawingAPI
    {
        public function drawCircle($x, $y, $radius)
        {
            echo "API2.cercle position $x:$y rayon $radius";
        }
    }

    /**
     * Abstraction
     */
    interface Shape
    {
        public function draw();
        /**
         *
         * @param double $pct
         */
        public function resizeByPercentage(double $pct);
    }

    /**
     * Abstraction raffinée
     */
    class CircleShape implements Shape
    {
        private $x, $y, $radius;
        /**
         *
         * @var DrawingAPI
         */
        private $drawingAPI;

        public function __construct(double $x, double $y, double $radius, DrawingAPI $drawingAPI)
        {
            $this->x = $x;
            $this->y = $y;
            $this->radius = $radius;
            $this->drawingAPI = $drawingAPI;

        }

        public function draw()
        {
            $this->drawingAPI->drawCircle($this->x, $this->y, $this->radius);
        }

        public function resizeByPercentage(double $pct)
        {
            $this->radius *= $pct;
        }

    }

    // Exemple d'utilisation
    $shapes = array();
    $shapes[] = new CircleShape(1, 2, 3, new DrawingAPI1());
    $shapes[] = new CircleShape(5, 7, 11, new DrawingAPI2());

    foreach ($shapes as $shape) {
        $shape->resizeByPercentage(2.5);
        $shape->draw();
    }
