<?php
// Exemple de code correspondant au diagramme

// interface des fenêtres
interface Window
{
    public function draw(); // dessine la fenêtre
    public function getDescription(); // retourne une description de la fenêtre
}

// implémentation d'une fenêtre simple, sans barre de défilement
class SimpleWindow implements Window
{
    public function draw()
    {
        // dessiner la fenêtre
    }

    public function getDescription()
    {
        return "fenêtre simple";
    }
}

// classe décorative abstraite, implémente Window
abstract class WindowDecorator implements Window
{
    protected $decoratedWindow; // la fenêtre décorée

    public function __construct(Window $decoratedWindow)
    {
        $this->decoratedWindow = $decoratedWindow;
    }
}

// décorateur concret ajoutant une barre verticale de défilement
class VerticalScrollBarDecorator extends WindowDecorator
{
    public function __construct(Window $decoratedWindow)
    {
        parent::__construct($decoratedWindow);
    }

    public function draw()
    {
        $this->drawVerticalScrollBar();
        $this->decoratedWindow->draw();
    }

    private function drawVerticalScrollBar()
    {
        // afficher la barre verticale de défilement
    }

    public function getDescription()
    {
        return decoratedWindow.getDescription() + ", avec une barre verticale de défilement";
    }
}


// décorateur concret ajoutant une barre horizontale de défilement
class HorizontalScrollBarDecorator extends WindowDecorator
{
    public function __construct(Window $decoratedWindow)
    {
        parent::__construct($decoratedWindow);
    }

    public function draw()
    {
        $this->drawHorizontalScrollBar();
        $this->decoratedWindow.draw();
    }

    private function drawHorizontalScrollBar()
    {
        // afficher la barre horizontale de défilement
    }

    public function getDescription()
    {
        return $this->decoratedWindow->getDescription() + ", avec une barre horizontale de défilement";
    }
}